import Instance from "../../scene/Instance";
import Renderable from "../../renderables/Renderable";

export default class BoxInstance extends Instance
{
    /**
     * Rotation on the x axis.
     */
    private x: number;

    /**
     * Rotation on the y axis.
     */
    private y: number;

    /**
     * Rotation on the z axis.
     */
    private z: number;

    /**
     * @param renderable
     */
    public constructor(renderable: Renderable)
    {
        super(renderable);

        this.x = Math.random();
        this.y = Math.random();
        this.z = Math.random();
    }

    /**
     * Rotate in any direction defined by random constructor values.
     */
    public animate() {
        this.rotateX(this.x);
        this.rotateY(this.y);
        this.rotateZ(this.z);
    }
}