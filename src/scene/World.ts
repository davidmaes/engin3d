import Camera from "./Camera";
import Instance from "./Instance";

export default class World
{
    /**
     * The WebGL rendering context.
     */
    private gl: WebGLRenderingContext;

    /**
     * The camera
     */
    private camera: Camera;

    /**
     * A list of instances to be rendered.
     */
    private instances: Instance[];

    /**
     * @param {WebGLRenderingContext} gl
     * @param camera
     */
    constructor(gl: WebGLRenderingContext, camera: Camera)
    {
        this.gl = gl;
        this.camera = camera;
        this.instances = [];
    }

    /**
     * Renders the world.
     */
    public render()
    {
        this.camera.clear();
        this.camera.renderInstances(this.instances);
    }

    /**
     * Adds an instance to the world.
     *
     * @param instance
     */
    public addInstance(instance: Instance) {
        this.instances.push(instance);
    }
}