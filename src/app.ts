import World from "./scene/World";
import Controller from "./controller/Controller";
import Camera from "./scene/Camera";
import Box from "./custom/renderables/Box";
import Matrix from "./math/Matrix";
import BoxInstance from "./custom/scene/BoxInstance";

class Main
{
    /**
     *
     */
    private renderCallback: FrameRequestCallback;

    /**
     *
     */
    private world: World;

    /**
     *
     */
    private camera: Camera;

    /**
     *
     */
    private controller: Controller;

    /**
     *
     */
    public constructor() {
        let canvas = <HTMLCanvasElement> document.getElementById('canvas');
        let gl = <WebGLRenderingContext> canvas.getContext('webgl');

        this.camera = new Camera(gl, canvas.width, canvas.height);
        this.world = new World(gl, this.camera);
        this.controller = new Controller(this.camera);
        this.renderCallback = this.gameLoop.bind(this);

        this.genesis(gl);
        this.gameLoop();
    }

    /**
     *
     */
    private gameLoop()
    {
        this.world.render();
        this.controller.onFrame();

        window.requestAnimationFrame(this.renderCallback);
    }

    /**
     * Create all the instances in the world.
     */
    public genesis(gl: WebGLRenderingContext)
    {
        let box = new Box(gl);
        box.uploadGraphics();

        for (let x = 0; x < 20; x++) {
            for (let z = 0; z < 20; z++) {
                let instance = new BoxInstance(box);
                instance.translate(-30 + x * 3, -10 + Math.random() * 20, z * -3);
                this.world.addInstance(instance);
            }
        }

        let m: Matrix = new Matrix();
        m.translate(0, 3, 2);
        this.camera.prepend(m);
    }
}

/**
 *
 */
addEventListener('load', function(){
    new Main();
});